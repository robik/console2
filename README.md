Status
------

| Feature                 | Windows | Linux | Usage
|-------------------------|:-------:|:-----:|-------------:
| _**Basic**_		  |         |       | 
| Foreground coloring	  | ✔       | ✔     | `console.foreground[=]`
| Background coloring	  | ✔       | ✔\*   | `console.background[=]`
| Text attributes	  | ✘       | ✔     | `console.textAttributes[=]`
| Clearing screen	  | ✔	    | ✔	    | `console.clear`
| Restoring original state| ✔	    | ✔     | `console.reset`
| _**Cursor**_            |         |       | 
| Fetching position	  | ✔       | ✔     | `console.cursor.position[=]`
| Manipulating position	  | ✔       | ✔     | `console.cursor.move*`
| Manipulating visibility | ✔       | ✔     | `console.cursor.visible[=]`
| Manipulating size	  | ✔       | ✘     | `console.cursor.size[=]`
| _**Window**_            |         |       |
| Setting title 	  | TODO    | TODO  |
| Fetching size	 	  | ✔       | ✔     | `console.size`
| _**Input**_             |         |       |
| Keyboard events 	  | ✔       | ✔     |
| Mouse events	 	  | ✔       | ✔\*\* |
| Resize events 	  | ✔       | ✔     |
| Interrupt event 	  | TODO    | ✔     |
| _**Other**_		  |         |       |
| Detecting console family| ✔       | ✔     | `console.name`, `console.inFamily(..)`
| State management	  | ✔	    | ✔	    | `console.wrap(..)`, `console.cursor.wrap(..)`



\* : Brightness in background is ignored

\*\* : Mouse movement event is available only on `xterm`, mouse button events are available on most emulators
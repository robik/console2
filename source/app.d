import std.stdio;
import std.string;
import console2;


alias Error = ConsoleTheme!(ConsoleColor.inherit, ConsoleColor.green);


void main()
{/+
    writeln("8 color rainbow:");
    foreach(bg; __traits(allMembers, ConsoleColor)[0..8]) {
        write(' ', bg.leftJustify(13));
    }
    writeln();
    
    foreach(fg; __traits(allMembers, ConsoleColor)[0..17]) {
        foreach(bg; __traits(allMembers, ConsoleColor)[0..8]) {
            console
				.fg(__traits(getMember, ConsoleColor, fg))
				.bg(__traits(getMember, ConsoleColor, bg))
            	.write(' ', fg.leftJustify(13))
				.reset();
        }
        writeln();
    }

	with(console.wrap()) console.fg(ConsoleColor.lightRed).writeln("Red");

	writeln("Foo ", Error("bar"), " !");
	//console.executionPolicy = ExecutionPolicy.manual;
	console.write("a ")
			.text(TextAttributes.strikethrough).write("b")
			.text(TextAttributes.underline).write(" c")
			.reset().writeln(" d");

	console.fg(ConsoleColor.lightYellow).write("eyyy ", Error("lmao")).reset().apply();

	//console.bg(ConsoleColor.initial).fg(ConsoleColor.initial).writeln(console.cursor.position);

	//console.clear();
	//console.cursor.moveTo((console.size/2));
	writeln("A");
	//console.cursor.moveTo(console.size);
	console.reset();+/

	Console.InputReader reader = Console.InputReader(console, ConsoleInputFlags.allInput);
	InputEvent ev;
	do {
		ev = reader.nextEvent();
		writeln(ev);
	} while(!(ev.type == InputEvent.Type.SpecialKeyEvent && ev.get!(InputEvent.Type.SpecialKeyEvent).key == SpecialKeyEvent.Key.Escape));

	//writeln(reader.getch());
}
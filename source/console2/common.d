module console2.common;

import std.algorithm;
import std.conv;
import std.stdio;
import std.string;
import std.typecons;

// TODO: lib name (console/terminal)
// TODO: termcap
// TODO: title (tricky cuz state)
// TODO: Signals
// TODO: Input poll
// TODO: Execution policy & cache policy for cursor
// TODO: Input lock
// TODO: unify linux/windows cursor behaviour
// TODO: wrap flags (cursor pos only/cursor flags only etc.)
// VERSION: CONSOLE2_COLOR_ALIAS
// VERSION: CONSOLE2_TERMINALD_COMPAT


/**
 * Console color.
 */
enum ConsoleColor : ushort
{        
    black        = 0, /// The black color.
    red          = 1, /// The blue color.
    green        = 2, /// The green color.
    blue         = 4, /// The red color.
    cyan         = blue | green, /// The cyan color. (blue-green)
    magenta      = blue | red, /// The magenta color. (dark pink like)
    yellow       = green | red, /// The yellow color.
    lightGray    = red | green | blue, /// The light gray color. (silver),
    silver       = lightGray,
    
    gray         = bright | black,  /// The gray color.
    lightRed     = bright | red,    /// The light red color.
    lightGreen   = bright | green,  /// The light green color.
    lightBlue    = bright | blue,   /// The light blue color.
    lightCyan    = bright | cyan,   /// The light cyan color. (light blue-green)
    lightMagenta = bright | magenta,/// The light magenta color. (pink)
    lightYellow  = bright | yellow, /// The light yellow color.
    white        = bright | silver, /// The white color.
    
    bright       = 128,  /// Bright flag. Use with dark colors to make them light equivalents.
    initial      = 256,  /// Default color.
	inherit		 = 512,	 /// Does not override current (e.g. for use in ConsoleTheme)
}


/**
 * Controls how are console actions executed.
 * 
 * Console actions are console color changes, cursor movement and options, input flags etc.
 */
enum ExecutionPolicy
{
	/**
	 * Action is always executed.
	 * 
	 * This is default.
	 */
	always = 1 << 0,


	/**
	 * Action is executed only when it is different from known state.
	 * 
	 * For example with this option, setting console foreground color to red twice will result ignore second call.
	 * Use with caution with `CachePolicy.always`.
	 */
	update = 1 << 1,


	/**
	 * Changes must be applied manually.
	 * 
	 * Example:
	 * 	console.fg(ConsoleColor.red).apply()
	 */
	manual = 1 << 2,


	/**
	 * State is never changed.
	 */
	never = 1 << 3,
}


/**
 * Controls how often system's console state is fetched.
 * 
 * This mostly affects set actions because most of them are re-fetching system's console state
 * update it and set it again.
 */
enum CachePolicy
{
	/**
	 * Data is always fetched.
	 * 
	 * This is default.
	 */
	never  = 1 << 0,


	/**
	 * Data is updated only when getting properties.
	 * 
	 * Using this value uses prefetched/cached state when setting properties.
	 * Can improve performance significally but may result in some subtle edge-cases bugs.
	 */
	always = 1 << 1
}


version(CONSOLE2_COLOR_ALIAS)
	public alias Color = ConsoleColor;


/**
 * Console text attributes.
 * 
 * These attributes take no effect on Windows console.
 */
enum TextAttributes
{
	/**
	 * No special effects.
	 * 
	 * This is default.
	 */
    none = 	0,


	/**
	 * Underlined text 
	 */
    underline = 1 << 0,


	/**
	 * Striked text
	 */
    strikethrough = 1 << 1,


	/**
	 * When overriding, text attribute is not changed.
	 */
	inherit = 1 << 2
}


struct ConsolePoint
{
	short x;
	short y;

	alias w = x;
	alias h = y;
	alias width = w;
	alias height = h;


	ConsolePoint opBinary(string op)(ConsolePoint rhs)
	{
		return mixin("ConsolePoint(cast(short)(x "~op~" rhs.x), cast(short)(y "~op~"rhs.y))");
	}

	ConsolePoint opBinary(string op)(short rhs)
	{
		return mixin("ConsolePoint(cast(short)(x "~op~" rhs), cast(short)(y "~op~"rhs))");
	}

	public string toString()
	{
		return "(%d, %d)".format(x, y);
	}
}


/**
 * Console current state.
 * 
 * Console save can be "saved" with `console.state` and later restored 
 * with `console.apply()`.
 * 
 * This struct only saves colors and text attributes, it does not save
 * console input state nor cursor position.
 * 
 * Examples:
 * ---
 * auto state = console.state;
 * // change console colors and text
 * console.apply(state);
 * ---
 * ---
 * with (console.wrap()) {
 * 	  // change console state
 * }
 * // restored
 * ---
 * ---
 * console.wrap({
 * 	  // change console state
 * })
 * // restored
 * ---
 */
struct ConsoleState
{
	/// Console foreground
	ConsoleColor foreground = ConsoleColor.inherit;

	/// Console background
	ConsoleColor background = ConsoleColor.inherit;

	/// Console text attributes
	TextAttributes textAttributes = TextAttributes.inherit;
}


enum ConsoleInputFlags : int
{
	none 	= 0,
	echo 	= 1 << 1,
	mouse 	= 1 << 2,
	window 	= 1 << 3,
	size 	= 1 << 4,
	paste 	= 1 << 5,
	winReleasedKeys = 1 << 8,

	allInput = mouse | window | size | paste,
	allInputWithWinReleasedKeys = allInput | winReleasedKeys
}


/// Input event for characters
struct CharacterEvent {
	/// .
	enum Type {
		Released, /// .
		Pressed /// .
	}
	
	Type eventType; /// .
	dchar character; /// .
	uint modifierState; /// Don't depend on this to be available for character events
}

struct SpecialKeyEvent {
	/// .
	enum Type {
		Released, /// .
		Pressed /// .
	}
	Type eventType; /// .
	
	// these match Windows virtual key codes numerically for simplicity of translation there
	//http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx
	/// .
	enum Key : int
	{
		Escape = 0x1b, /// .
		F1 = 0x70, /// .
		F2 = 0x71, /// .
		F3 = 0x72, /// .
		F4 = 0x73, /// .
		F5 = 0x74, /// .
		F6 = 0x75, /// .
		F7 = 0x76, /// .
		F8 = 0x77, /// .
		F9 = 0x78, /// .
		F10 = 0x79, /// .
		F11 = 0x7A, /// .
		F12 = 0x7B, /// .
		LeftArrow = 0x25, /// .
		RightArrow = 0x27, /// .
		UpArrow = 0x26, /// .
		DownArrow = 0x28, /// .
		Insert = 0x2d, /// .
		Delete = 0x2e, /// .
		Home = 0x24, /// .
		End = 0x23, /// .
		PageUp = 0x21, /// .
		PageDown = 0x22, /// .
	}
	Key key; /// .
	
	uint modifierState; /// A mask of ModifierState. Always use by checking modifierState & ModifierState.something, the actual value differs across platforms	
}

version(CONSOLE2_TERMINALD_COMPAT)
	deprecated("Use SpecialKeyEvent instead") alias NonCharacterKeyEvent = SpecialKeyEvent;

/// .
struct PasteEvent {
	string pastedText; /// .
}

/// .
struct MouseEvent {
	// these match arsd/simpledisplay.d numerically as well
	/// .
	enum Type {
		Moved = 0, /// .
		Pressed = 1, /// .
		Released = 2, /// .
		Clicked = 4, /// .
	}
	
	Type eventType; /// .
	
	// note: these should numerically match arsd/simpledisplay.d for maximum beauty in my other code
	/// .
	enum Button : uint {
		None = 0, /// .
		Left = 1, /// .
		Middle = 4, /// .
		Right = 2, /// .
		ScrollUp = 8, /// .
		ScrollDown = 16 /// .
	}
	uint buttons; /// A mask of Button
	ConsolePoint position;
	uint modifierState; /// shift, ctrl, alt, meta, altgr. Not always available. Always check by using modifierState & ModifierState.something
}

/// .
struct SizeChangedEvent {
	ConsolePoint prev;
	ConsolePoint curr;
}

/// the user hitting ctrl+c will send this
/// You should drop what you're doing and perhaps exit when this happens.
struct UserInterruptionEvent {}

/// If the user hangs up (for example, closes the terminal emulator without exiting the app), this is sent.
/// If you receive it, you should generally cleanly exit.
struct HangupEvent {}

/// Sent upon receiving end-of-file from stdin.
struct EndOfFileEvent {}

interface CustomEvent {}

version(Windows)
enum ModifierState : uint {
	shift = 0x10,
	control = 0x8 | 0x4, // 8 == left ctrl, 4 == right ctrl
	
	// i'm not sure if the next two are available
	alt = 2 | 1, //2 ==left alt, 1 == right alt
	
	// FIXME: I don't think these are actually available
	windows = 512,
	meta = 4096, // FIXME sanity
	
	// I don't think this is available on Linux....
	scrollLock = 0x40,
}
else
enum ModifierState : uint {
	shift = 4,
		alt = 2,
		control = 16,
		meta = 8,
		
		windows = 512 // only available if you are using my terminal emulator; it isn't actually offered on standard linux ones
}


struct InputEvent
{
	enum Type
	{
		CharacterEvent, ///.
		SpecialKeyEvent, /// .
		PasteEvent, /// The user pasted some text. Not always available, the pasted text might come as a series of character events instead.
		MouseEvent, /// only sent if you subscribed to mouse events
		SizeChangedEvent, /// only sent if you subscribed to size events
		UserInterruptionEvent, /// the user hit ctrl+c
		EndOfFileEvent, /// stdin has received an end of file
		HangupEvent, /// the terminal hanged up - for example, if the user closed a terminal emulator
		CustomEvent /// .

	}

	protected Type t;	
	union
	{
		CharacterEvent characterEvent;
		SpecialKeyEvent specialKeyEvent;
		PasteEvent pasteEvent;
		MouseEvent mouseEvent;
		SizeChangedEvent sizeChangedEvent;
		UserInterruptionEvent userInterruptionEvent;
		HangupEvent hangupEvent;
		EndOfFileEvent endOfFileEvent;
		CustomEvent customEvent;
	}

	this(CharacterEvent c)
	{
		t = Type.CharacterEvent;
		characterEvent = c;
	}

	this(SpecialKeyEvent c)
	{
		t = Type.SpecialKeyEvent;
		specialKeyEvent = c;
	}

	this(PasteEvent c)
	{
		t = Type.PasteEvent;
		pasteEvent = c;
	}

	this(MouseEvent c)
	{
		t = Type.MouseEvent;
		mouseEvent = c;
	}

	this(SizeChangedEvent c)
	{
		t = Type.SizeChangedEvent;
		sizeChangedEvent = c;
	}

	this(UserInterruptionEvent c)
	{
		t = Type.UserInterruptionEvent;
		userInterruptionEvent = c;
	}

	this(HangupEvent c)
	{
		t = Type.HangupEvent;
		hangupEvent = c;
	}

	this(EndOfFileEvent c)
	{
		t = Type.EndOfFileEvent;
		endOfFileEvent = c;
	}

	this(CustomEvent c)
	{
		t = Type.CustomEvent;
		customEvent = c;
	}

	/// .
	@property Type type() { return t; }
	
	/// .
	@property auto get(Type T)() {
		if(type != T)
			throw new Exception("Wrong event type");
		static if(T == Type.CharacterEvent)
			return characterEvent;
		else static if(T == Type.SpecialKeyEvent)
			return specialKeyEvent;
		else static if(T == Type.PasteEvent)
			return pasteEvent;
		else static if(T == Type.MouseEvent)
			return mouseEvent;
		else static if(T == Type.SizeChangedEvent)
			return sizeChangedEvent;
		else static if(T == Type.UserInterruptionEvent)
			return userInterruptionEvent;
		else static if(T == Type.EndOfFileEvent)
			return endOfFileEvent;
		else static if(T == Type.HangupEvent)
			return hangupEvent;
		else static if(T == Type.CustomEvent)
			return customEvent;
		else static assert(0, "Type " ~ T.stringof ~ " not added to the get function");
	}

	public string toString()
	{
		string inner;

		switch(t) with (Type)
		{
			case CharacterEvent:
				inner = to!string(characterEvent);
			break;

			case SpecialKeyEvent:
				inner = to!string(specialKeyEvent);
			break;
				
			case MouseEvent:
				inner = to!string(mouseEvent);
				break;
			
			case PasteEvent:
				inner = to!string(pasteEvent);
			break;
			
			case SizeChangedEvent:
				inner = to!string(sizeChangedEvent);
			break;
				
			case UserInterruptionEvent:
				inner = to!string(userInterruptionEvent);
			break;
			
			case HangupEvent:
				inner = to!string(hangupEvent);
			break;
			
			case EndOfFileEvent:
				inner = to!string(endOfFileEvent);
			break;
			
			case CustomEvent:
				inner = to!string(customEvent);
			break;
				
			default:
				assert(0, "Invalid event type");
		}
		
		return "%s(%s)".format(typeof(this).stringof, inner);
	}
}


struct ConsoleStateWrap
{
	AbstractConsole console;
	ConsoleState state;

	~this()
	{
		console.apply(state);
	}
}

struct ConsoleCursorStateWrap
{
	ConsoleCursor cursor;
	ConsolePoint pos;
	
	~this()
	{
		cursor.position = pos;
	}
}
/*
struct ConsoleInputFlagsWrap
{
	ConsoleInput input;
	ConsoleInputFlags flags;
	
	~this()
	{
		input.apply(flags);
	}
}*/


/**
 * Console theme.
 * 
 * Allows for easy coloring output with builtin IO functions.
 * 
 * Examples:
 * ---
 * alias Error = ConsoleTheme!(ConsoleColor.lightRed);
 * 
 * void main()
 * {
 *     writeln(Error("Error"), ": normal text");
 * }
 * ---
 */
struct ConsoleTheme(ConsoleColor fg, ConsoleColor bg, TextAttributes text = TextAttributes.inherit)
{
	import console2; // for global console

	string s;
	this(string s)
	{
		this.s = s;
	}
	
	void toString(scope void delegate(const(char)[]) sink) const
	{
		with(console.wrap()) {
			console.apply(ConsoleState(fg, bg, text));
			sink(s.dup);
		}
	}
}


enum ConsoleFamily
{
	windows = "windows",
	xterm = "xterm",
	screen = "screen",
	rxvt = "rxvt",
}


abstract class AbstractConsole
{
    protected File _stream;
    protected ConsoleState _state;
    protected ConsoleCursor _cursor;


	/**
	 * If set to false, console will not be reset at program exit.
	 */
	public bool restoreAtExit = true;


	/**
	 * Data cache policy
	 * 
	 * This option affects console only (not input and cursor yet).
	 * 
	 * More information on options behavior is described in `CachePolicy` documentation.
	 * 
	 * Default value: `CachePolicy.never`
	 */
	public CachePolicy cachePolicy = CachePolicy.never;


	/**
	 * Execution policy.
	 * 
	 * This options affects only console (not input and cursor yet).
	 * 
	 * More information can be found in `ExecutionPolicy` documentation.
	 * 
	 * Default vaulue: `ExecutionPolicy.always`
	 */
	public ExecutionPolicy executionPolicy = ExecutionPolicy.always;
    
    
    public this(File stream = stdout)
    {
        this._stream = stream;
    }
    
    
    //-------------------------------------------------------------------------- GETTERS
	/**
	 * Console name.
	 */
	@property
	public abstract string name() const;

	/**
	 * Gets console state.
	 */
    @property
    public const ConsoleState state()
    {
        return _state;
    }
    
    
	/**
	 * Gets output stream.
	 */
    @property
    public File outputStream()
    {
        return _stream;
    }
    

    /**
     * Gets foreground color.
     */
    @property
    public ConsoleColor foreground() const
    {
        return _state.foreground;
    }

	/// ditto
    public alias fg = foreground;
    
    	
	/**
     * Gets background color.
     */
    @property
    public ConsoleColor background() const
    {
        return _state.background;
    }

	/// ditto
    public alias bg = background;
    
    	
	/**
     * Gets text attributes.
     */
    @property
	public TextAttributes textAttributes() const
    {
		return _state.textAttributes;
    }

	/// ditto
	alias text = textAttributes;
	

	/**
     * Console cursor object.
     */
	@property
	public ConsoleCursor cursor()
	{
		return _cursor;
	}
	
	
	/**
	 * Gets console window size in characters.
	 */
	@property
	public abstract ConsolePoint size();
    
    
    //-------------------------------------------------------------------------- SETTERS
	/**
	 * Sets output stream.
	 */
	@property
	public void outputStream(File stream)
	{
		_stream = stream;
	}


	/**
	 * Sets foreground color.
	 */
	@property
	public typeof(this) foreground(ConsoleColor fg)
	{
		if (fg == ConsoleColor.inherit) return this;

		_state.foreground = fg;
		if (executionPolicy != ExecutionPolicy.manual)
			apply(state);

		return this;
	}	

	
	/**
	 * Sets background color.
	 */
	@property
	public typeof(this) background(ConsoleColor bg)
	{        
		if (bg == ConsoleColor.inherit) return this;

		_state.background = bg;
		if (executionPolicy != ExecutionPolicy.manual)
			apply(state);

		return this;
	}

	
	/**
	 * Sets text attributes.
	 */
	@property
	public typeof(this) textAttributes(TextAttributes newTextAttributes)
	{
		if (newTextAttributes == TextAttributes.inherit) return this;

		_state.textAttributes = newTextAttributes;
		if (executionPolicy != ExecutionPolicy.manual)
			apply(state);
		
		return this;
	}


	//-------------------------------------------------------------------------- METHODS
	/**
	 * Clears screen and moves cursor to (0, 0).
	 */
	public abstract typeof(this) clear();


	/**
	 * Resets text color and attributes.
	 */
	public typeof(this) reset()
	{
		return apply(ConsoleState(ConsoleColor.initial, ConsoleColor.initial, TextAttributes.none));
	}


	/**
	 * Applies current state.
	 * 
	 * Should be used only when `executionPolicy` is `ExecutionPolicy.manual` as
	 * `foreground`, `background` and `textAttributes` properties automatically
	 * apply state.
	 */
	public typeof(this) apply()
	{
		return apply(_state);
	}


	/**
	 * Applies specified state and sets it to current.
	 */
	public typeof(this) apply(const ConsoleState newState)
	{
		if (executionPolicy == ExecutionPolicy.never)
			return this;

		bool doApply = (executionPolicy != ExecutionPolicy.update);

		if (executionPolicy == ExecutionPolicy.update && _state != newState)
			doApply = true;

		if (newState.foreground != ConsoleColor.inherit)
			_state.foreground = newState.foreground;

		if (newState.background != ConsoleColor.inherit)
			_state.background = newState.background;
		
		if (newState.textAttributes != TextAttributes.inherit)
			_state.textAttributes = newState.textAttributes;

		if (doApply)
			this.applyState(_state);

		return this;
	}
    

	protected abstract void applyState(const ConsoleState state);


	//-------------------------------------------------------------------------- UTILS
	/**
	 * Restores state from before executing `dg`.
	 */
	public typeof(this) wrap(void delegate() dg)
	{
		ConsoleState backState = state;
		dg();
		apply(backState);

		return this;
	}


	/**
	 * Restores struct that restores saved `ConsoleState` on destruction.
	 * 
	 * Examples:
	 * ---
	 * with(console.wrap())
	 * {
	 * 	   // colors and text attribute changes will be restored after leaving this scope
	 * }
	 * ---
	 */
	public ConsoleStateWrap wrap()
	{
		return ConsoleStateWrap(this, state);
	}


	/**
	 * Checks if this console is on of specified (family)
	 */
	public bool inFamily(string[] names...)
	{
		return names.canFind(name);
	}

	version(CONSOLE2_TERMINALD_COMPAT)
		deprecated("Use 'inFamily' instead") alias terminalInFamily = inFamily;


	//-------------------------------------------------------------------------- STREAM PROXY
	/**
	 * Alias for `stdio.write` that returns console.
	 */
	public typeof(this) write(T...)(T args)
	{
		_stream.write(args);

		return this;
	}


	/**
	 * Alias for `stdio.writef` that returns console.
	 */
	public typeof(this) writef(T...)(string fmt, T args)
	{
		_stream.writef(fmt, args);
		
		return this;
	}


	/**
	 * Alias for `stdio.writeln` that returns console.
	 */
	public typeof(this) writeln(T...)(T args)
	{
		_stream.writeln(args);
		
		return this;
	}
	

	/**
	 * Alias for `stdio.writefln` that returns console.
	 */
	public typeof(this) writefln(T...)(string fmt, T args)
	{
		_stream.writefln(fmt, args);
		
		return this;
	}
}


abstract class ConsoleCursor
{
	@property public abstract bool visible();
	@property public abstract ConsoleCursor visible(bool isVisible);
	@property public abstract uint size();
	@property public abstract ConsoleCursor size(uint newSize);
	@property public abstract ConsolePoint position();
	public abstract ConsoleCursor move(const ConsolePoint pos);
	public abstract ConsoleCursor moveTo(const ConsolePoint pos);


	//-------------------------------------------------------------------------- UTILS
	@property
	public ConsoleCursor position(ConsolePoint pos)
	{
		moveTo(position);

		return this;
	}


	public ConsoleCursor moveTo(ushort x, ushort y)
	{
		moveTo(ConsolePoint(x, y));
		
		return this;
	}


	public ConsoleCursor move(short x, short y)
	{
		move(ConsolePoint(x, y));
		
		return this;
	}
    
    
	public ConsoleCursor moveUp(ushort y = 1)
    {
        this.move(0, -y);
		
		return this;
    }
    
    
	public ConsoleCursor moveDown(ushort y = 1)
    {
        this.move(0, y);
		
		return this;
    }
    

	public ConsoleCursor moveLeft(ushort x = 1)
    {
        this.move(-x, 0);

		return this;
    }
    
    
	public ConsoleCursor moveRight(ushort x = 1)
    {
        this.move(x, 0);

		return this;
    }
	
	

	public ConsoleCursor wrap(void delegate() dg)
	{
		ConsolePoint backPos = position;
		dg();
		position = backPos;
		
		return this;
	}
	
	
	public ConsoleCursorStateWrap wrap()
	{
		return ConsoleCursorStateWrap(this, position);
	}
}

/*
class ConsoleInput
{
	protected ConsoleInputFlags _defaultInputFlags;
	protected ConsoleInputFlags _inputFlags;


	@property
	public const ConsoleInputFlags flags()
	{
		return _inputFlags;
	}


	@property
	public ConsoleInput flags(ConsoleInputFlags flags)
	{
		_inputFlags = flags;
		apply(flags);

		return this;
	}
	
	
	@property
	public bool echo() const
	{
		return !!(_inputFlags & ConsoleInputFlags.echo);
	}
	
	@property
	public ConsoleInput echo(bool echo)
	{
		echo ? (_inputFlags |= ConsoleInputFlags.echo) : (_inputFlags &= ~ConsoleInputFlags.echo);
		apply(_inputFlags);
		
		return this;
	}
	
	
	@property
	public bool raw() const
	{
		return !!(_inputFlags & ConsoleInputFlags.raw);
	}
	
	@property
	public ConsoleInput raw(bool lineBuffering)
	{
		echo ? (_inputFlags |= ConsoleInputFlags.raw) : (_inputFlags &= ~ConsoleInputFlags.raw);
		apply(_inputFlags);
		
		return this;
	}


	public ConsoleInput reset()
	{
		return apply(_defaultInputFlags);
	}


	public abstract bool waitForEvent(int milliseconds);
	public abstract ConsoleInput apply(const ConsoleInputFlags flags);
	
		
	public ConsoleInput wrap(void delegate() dg)
	{
		ConsoleInputFlags backFlags = this.flags;
		dg();
		apply(backFlags);
		
		return this;
	}
	
	
	public ConsoleInputFlagsWrap wrap()
	{
		return ConsoleInputFlagsWrap(this, flags);
	}


	public bool kbhit()
	{
		return waitForEvent(0);
	}
}*/
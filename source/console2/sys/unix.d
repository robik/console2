module console2.sys.unix;

version(linux):
import core.sys.posix.fcntl;
import core.sys.posix.unistd;
import core.sys.posix.termios;
import core.sys.posix.signal;
import core.sys.posix.sys.ioctl;
import core.sys.posix.sys.time;
import std.conv;
import std.math;
import std.process;
import std.stdio;
import std.string;
import console2.common;


private enum ANSI
{
    UNDERLINE_ENABLE  = 4,
    UNDERLINE_DISABLE = 24,
        
    STRIKE_ENABLE     = 9,
    STRIKE_DISABLE    = 29
}

private enum SIGWINCH = 28;


class UnixConsole : AbstractConsole
{
	// Cached stdin and specified output file fds
	package int _inputFd;
	package int _outputFd;
	package ConsolePoint _sizeCache;


    this(File outputStream = stdout)
    {
        super(outputStream);
		_inputFd = stdin.fileno;
		_outputFd = stdout.fileno;
        _cursor = new UnixConsole.Cursor();
		_sizeCache = size;
    }


    //-------------------------------------------------------------------------- GETTERS
	@property
	public override string name() const
	{
		return environment.get("TERM");
	}


    @property
    public bool isOutputRedirected()
    {
        // returns 1 if fd is an open file descriptor referring to a terminal
		return isatty(_outputFd) != 1;
    }


	@property 
	public override ConsolePoint size()
	{
		winsize w;
		ioctl(_inputFd, TIOCGWINSZ, &w);
		
		return ConsolePoint(cast(int)w.ws_col, cast(int)w.ws_row);
	}
        
	
	
	/**
	 * Gets output stream.
	 */
	@property
	public override File outputStream()
	{
		return _stream;
	}

    
    //-------------------------------------------------------------------------- SETTERS
	@property
	public override void outputStream(File stream)
	{
		_stream = stream;
		_outputFd = stream.fileno;
	}


	//-------------------------------------------------------------------------- METHODS
	public override typeof(this) clear()
	{
		_writeAnsi("2J");

		return this;
	}


    protected override void applyState(const ConsoleState state)
    {
        _writeAnsi("0;%d;%d;%dm",
            (fg & ConsoleColor.bright) ? 1 : 0,
            
			(state.textAttributes & TextAttributes.underline)     ? ANSI.UNDERLINE_ENABLE : ANSI.UNDERLINE_DISABLE,
			(state.textAttributes & TextAttributes.strikethrough) ? ANSI.STRIKE_ENABLE    : ANSI.STRIKE_DISABLE
        );

		if (_state.foreground != ConsoleColor.initial)
			_writeAnsi("%dm", _toAnsiColor(fg));

		if (_state.background != ConsoleColor.initial)
			_writeAnsi("%dm", _toAnsiColor(bg) + 10); // Background colors are normal + 10
    }
    
    
	protected ushort _toAnsiColor(ConsoleColor color)
    {
        ushort raw = (color & ~ConsoleColor.bright);
        return cast(ushort)(raw + 30);
    }

	package void _writeAnsi(T...)(string fmt, T args)
	{
		if (isOutputRedirected) return;

		_stream.writef("\033[" ~ fmt, args);
	}
	
	
	
	//-------------------------------------------------------------------------- CURSOR
	class Cursor : ConsoleCursor
	{
		// We assume that cursor is visible by default
		protected bool _visible = true;


		@property
		public override bool visible()
		{
			return _visible;
		}
		
		
		@property
		public override ConsoleCursor visible(bool isVisible)
		{
			_visible = isVisible;
			_writeAnsi("?25%c", isVisible ? 'h' : 'l');
			return this;
		}


		@property 
		public override uint size()
		{
			return 100;
		}

		@property 
		public override ConsoleCursor size(uint newSize)
		{
			return this;
		}

		@property 
		public override ConsolePoint position()
		{
			termios told;
			byte[128] __hack1;
			termios tnew;
			byte[128] __hack2;

			tcgetattr(_inputFd, &told);
			tnew = told;
			tnew.c_lflag &= ~ECHO & ~ICANON;
			tcsetattr(_inputFd, TCSANOW, &tnew);

			char[] buf;
			char[][] tmp;
			buf.reserve(8);

			_writeAnsi("6n");
			stdout.flush();
			foreach(i; 0..8)
			{
				char c;
				c = cast(char)getchar();
				buf ~= c;
				if(c == 'R')
					break;
			}
			tcsetattr(_inputFd, TCSANOW, &told);
			
			buf = buf[2..$-1];
			tmp = buf.split(";");
			
			return ConsolePoint(cast(short)(to!short(tmp[1]) - 1), cast(short)(to!short(tmp[0]) - 1));
		}
		
		
		public override ConsoleCursor moveTo(const ConsolePoint pos)
		{
			_stream.flush();
			_writeAnsi("%d;%df", pos.y + 1, pos.x + 1);
			
			return this;
		}
		
		
		public override ConsoleCursor move(const ConsolePoint delta)
		{
			_stream.flush();
			_writeAnsi("%d%c", delta.x.abs, (delta.x < 0) ? 'D' : 'C');
			_writeAnsi("%d%c", delta.y.abs, (delta.y < 0) ? 'A' : 'B');
			
			return this;
		}
		
		
		public override ConsoleCursor moveUp(ushort y = 1)
		{
			_writeAnsi("%dA", y);
			_stream.flush();
			
			return this;
		}
		
		
		public override ConsoleCursor moveDown(ushort y = 1)
		{
			_writeAnsi("%dB", y);
			_stream.flush();
			
			return this;
		}
		
		
		public override ConsoleCursor moveLeft(ushort x = 1)
		{
			_writeAnsi("%dD", x);
			_stream.flush();
			
			return this;
		}
		
		
		public override ConsoleCursor moveRight(ushort x = 1)
		{
			_writeAnsi("%dC", x);
			_stream.flush();
			
			return this;
		}
	}


	static struct InputReader
	{
		protected Console _console;
		protected termios _defaultTio;
		protected ubyte[128] __hack1;
		protected termios _tio;
		protected ubyte[128] __hack2;
		protected ConsoleInputFlags _flags;
		protected sigaction_t _oldSigWinch;
		protected sigaction_t _oldSigIntr;
		protected sigaction_t _oldHupIntr;
		protected InputEvent[] _inputQueue;
		protected __gshared bool windowSizeChanged = false;
		protected __gshared bool interrupted = false; 
		protected __gshared bool hangedUp = false;
		protected int delegate(char) _inputPrefilter;
		protected bool _closed;


		extern(C)
		static void _sizeSignalHandler(int sigNumber) nothrow
		{
			windowSizeChanged = true;
		}

		extern(C)
		static void _interruptSignalHandler(int sigNumber) nothrow
		{
			interrupted = true;
		}

		extern(C)
		static void _hangupSignalHandler(int sigNumber) nothrow
		{
			hangedUp = true;
		}


		public this(UnixConsole console, ConsoleInputFlags flags)
		{
			_console = console;
			_flags = flags;
			tcgetattr(console._inputFd, &_tio);
			_defaultTio = _tio;

			_tio.c_lflag = _tio.c_lflag & ~(ECHO | ICANON);
			if (_flags & ConsoleInputFlags.echo)
				_tio.c_lflag |= ECHO;

			tcsetattr(_console._inputFd, TCSANOW, &_tio);

			if(flags & ConsoleInputFlags.size) {
				sigaction_t n;
				n.sa_handler = &_sizeSignalHandler;
				n.sa_mask = cast(sigset_t) 0;
				n.sa_flags = 0;
				sigaction(SIGWINCH, &n, &_oldSigWinch);
			}

			if(_flags & ConsoleInputFlags.mouse) {
				// basic button press+release notification
				
				// FIXME: try to get maximum capabilities from all terminals
				// right now this works well on xterm but rxvt isn't sending movements...
				
				_console._writeAnsi("?1000h");
				if(_console.inFamily("xterm")) {
					// this is vt200 mouse with full motion tracking, supported by xterm
					_console._writeAnsi("?1003h");
				} else if(_console.inFamily("rxvt", "screen")) {
					_console._writeAnsi("?1002h"); // this is vt200 mouse with press/release and motion notification iff buttons are pressed
				}
			}
		}

		public ~this()
		{
			this.close();
		}

		public void close()
		{
			if (_closed) return;

			tcsetattr(_console._inputFd, TCSANOW, &_defaultTio);

			if (_flags & ConsoleInputFlags.size) {
				sigaction(SIGWINCH, &_oldSigWinch, null);
			}

			if (_flags & ConsoleInputFlags.mouse) {
				_console._writeAnsi("?1000l");
			}

			if(_flags & ConsoleInputFlags.mouse) {
				if(_console.inFamily("xterm"))
					_console._writeAnsi("?1003l");
				else if(_console.inFamily("rxvt", "screen"))
					_console._writeAnsi("?1002l");
			}

			_closed = true;
		}

		bool kbhit(int ms = 0)
		{
			return timedCheckForInput(ms);
		}


		dchar getch()
		{
			auto event = nextEvent();
			while(event.type != InputEvent.Type.CharacterEvent || event.characterEvent.eventType == CharacterEvent.Type.Released) {
				if(event.type == InputEvent.Type.UserInterruptionEvent)
					throw new Exception("Ctrl+c");
				if(event.type == InputEvent.Type.HangupEvent)
					throw new Exception("Hangup");
				if(event.type == InputEvent.Type.EndOfFileEvent)
					return dchar.init;
				event = nextEvent();
			}
			return event.characterEvent.character;
		}

				
		InputEvent nextEvent()
		{
			_console.outputStream.flush();
			
			if(_inputQueue.length) {
				auto e = _inputQueue[0];
				_inputQueue = _inputQueue[1 .. $];
				return e;
			}
			
		wait_for_more:
			if(interrupted) {
				interrupted = false;
				return InputEvent(UserInterruptionEvent());
			}
			
			if(hangedUp) {
				hangedUp = false;
				return InputEvent(HangupEvent());
			}
			
			if(windowSizeChanged) {
				ConsolePoint oldSize = _console._sizeCache;
				_console._sizeCache = _console.size;
				return InputEvent(SizeChangedEvent(oldSize, _console._sizeCache));
			}
			
			auto more = _readNextEvents();
			if(!more.length)
				// i used to do a loop (readNextEvents can read something, but it might be discarded by the input filter) but now 
				// its goto's above because readNextEvents might be interrupted by a SIGWINCH aka size event so we want to check that at least
				goto wait_for_more; 
			
			assert(more.length);
			
			auto e = more[0];
			_inputQueue = more[1 .. $];
			return e;
		}


		bool timedCheckForInput(int milliseconds)
		{
			_console.outputStream.flush();
			if(_console._inputFd == -1)
				return false;
			
			timeval tv;
			tv.tv_sec = 0;
			tv.tv_usec = milliseconds * 1000;
			
			fd_set fs;
			FD_ZERO(&fs);
			
			FD_SET(_console._inputFd, &fs);
			select(_console._inputFd + 1, &fs, null, null, &tv);
			
			return FD_ISSET(_console._inputFd, &fs);
		}


		protected int _nextRaw(bool interruptable = false)
		{
			if(_console._inputFd == -1)
				return 0;
			
			char[1] buf;
		try_again:
			auto ret = read(_console._inputFd, buf.ptr, buf.length);
			if(ret == 0)
				return 0; // input closed
			if(ret == -1) {
				import core.stdc.errno;
				if(errno == EINTR)
					// interrupted by signal call, quite possibly resize or ctrl+c which we want to check for in the event loop
					if(interruptable)
						return -1;
					else
						goto try_again;
				else
					throw new Exception("read failed");
			}
			
			if(ret == 1)
				return _inputPrefilter ? _inputPrefilter(buf[0]) : buf[0];
			else
				assert(0); // read too much, should be impossible
		}


		protected InputEvent[] _readNextEvents()
		{
			_console.outputStream.flush(); // make sure all output is sent out before we try to get input
			
			// we want to starve the read, especially if we're called from an edge-triggered
			// epoll (which might happen in version=with_eventloop.. impl detail there subject
			// to change).
			auto initial = _readNextEventsHelper();
			
			// lol this calls select() inside a function prolly called from epoll but meh,
			// it is the simplest thing that can possibly work. The alternative would be
			// doing non-blocking reads and buffering in the nextRaw function (not a bad idea
			// btw, just a bit more of a hassle).
			while(timedCheckForInput(0))
				initial ~= _readNextEventsHelper();

			return initial;
		}


		protected dchar _nextUtf8Char(int starting)
		{
			if(starting <= 127)
				return cast(dchar) starting;
			char[6] buffer;
			int pos = 0;
			buffer[pos++] = cast(char) starting;

			// see the utf-8 encoding for details
			int remaining = 0;
			ubyte magic = starting & 0xff;
			while(magic & 0b1000_000) {
				remaining++;
				magic <<= 1;
			}
			
			while(remaining && pos < buffer.length) {
				buffer[pos++] = cast(char) _nextRaw();
				remaining--;
			}
			
			import std.utf;
			size_t throwAway; // it insists on the index but we don't care
			return decode(cast(string)buffer, throwAway);
		}


		
		// The helper reads just one actual event from the pipe...
		protected InputEvent[] _readNextEventsHelper()
		{
			InputEvent[] charPressAndRelease(dchar character) {
				if((_flags & ConsoleInputFlags.winReleasedKeys))
					return [
					InputEvent(CharacterEvent(CharacterEvent.Type.Pressed, character, 0)),
					InputEvent(CharacterEvent(CharacterEvent.Type.Released, character, 0)),
				];
				else return [ InputEvent(CharacterEvent(CharacterEvent.Type.Pressed, character, 0)) ];
			}

			InputEvent[] keyPressAndRelease(SpecialKeyEvent.Key key, uint modifiers = 0) {
				if((_flags & ConsoleInputFlags.winReleasedKeys))
					return [
					InputEvent(SpecialKeyEvent(SpecialKeyEvent.Type.Pressed, key, modifiers)),
					InputEvent(SpecialKeyEvent(SpecialKeyEvent.Type.Released, key, modifiers)),
				];
				else return [ InputEvent(SpecialKeyEvent(SpecialKeyEvent.Type.Pressed, key, modifiers)) ];
			}
			
			char[30] sequenceBuffer;
			
			// this assumes you just read "\033["
			char[] readEscapeSequence(char[] sequence) {
				int sequenceLength = 2;
				sequence[0] = '\033';
				sequence[1] = '[';
				
				while(sequenceLength < sequence.length) {
					auto n = _nextRaw();
					sequence[sequenceLength++] = cast(char) n;
					// I think a [ is supposed to termiate a CSI sequence
					// but the Linux console sends CSI[A for F1, so I'm
					// hacking it to accept that too
					if(n >= 0x40 && !(sequenceLength == 3 && n == '['))
						break;
				}
				
				return sequence[0 .. sequenceLength];
			}
			
			InputEvent[] translateTermcapName(string cap) {
				switch(cap) {
					//case "k0":
					//return keyPressAndRelease(NonCharacterKeyEvent.Key.F1);
					case "k1":
						return keyPressAndRelease(SpecialKeyEvent.Key.F1);
					case "k2":
						return keyPressAndRelease(SpecialKeyEvent.Key.F2);
					case "k3":
						return keyPressAndRelease(SpecialKeyEvent.Key.F3);
					case "k4":
						return keyPressAndRelease(SpecialKeyEvent.Key.F4);
					case "k5":
						return keyPressAndRelease(SpecialKeyEvent.Key.F5);
					case "k6":
						return keyPressAndRelease(SpecialKeyEvent.Key.F6);
					case "k7":
						return keyPressAndRelease(SpecialKeyEvent.Key.F7);
					case "k8":
						return keyPressAndRelease(SpecialKeyEvent.Key.F8);
					case "k9":
						return keyPressAndRelease(SpecialKeyEvent.Key.F9);
					case "k;":
					case "k0":
						return keyPressAndRelease(SpecialKeyEvent.Key.F10);
					case "F1":
						return keyPressAndRelease(SpecialKeyEvent.Key.F11);
					case "F2":
						return keyPressAndRelease(SpecialKeyEvent.Key.F12);
						
						
					case "kb":
						return charPressAndRelease('\b');
					case "kD":
						return keyPressAndRelease(SpecialKeyEvent.Key.Delete);
						
					case "kd":
					case "do":
						return keyPressAndRelease(SpecialKeyEvent.Key.DownArrow);
					case "ku":
					case "up":
						return keyPressAndRelease(SpecialKeyEvent.Key.UpArrow);
					case "kl":
						return keyPressAndRelease(SpecialKeyEvent.Key.LeftArrow);
					case "kr":
					case "nd":
						return keyPressAndRelease(SpecialKeyEvent.Key.RightArrow);
						
					case "kN":
					case "K5":
						return keyPressAndRelease(SpecialKeyEvent.Key.PageDown);
					case "kP":
					case "K2":
						return keyPressAndRelease(SpecialKeyEvent.Key.PageUp);
						
					case "kh":
					case "K1":
						return keyPressAndRelease(SpecialKeyEvent.Key.Home);
					case "kH":
						return keyPressAndRelease(SpecialKeyEvent.Key.End);
					case "kI":
						return keyPressAndRelease(SpecialKeyEvent.Key.Insert);
					default:
						// don't know it, just ignore
						//import std.stdio;
						//writeln(cap);
				}
				
				return null;
			}
			
			
			InputEvent[] doEscapeSequence(in char[] sequence) {
				switch(sequence) {
					case "\033[200~":
						// bracketed paste begin
						// we want to keep reading until
						// "\033[201~":
						// and build a paste event out of it
						string data;
						for(;;) {
							auto n = _nextRaw();
							if(n == '\033') {
								n = _nextRaw();
								if(n == '[') {
									auto esc = readEscapeSequence(sequenceBuffer);
									if(esc == "\033[201~") {
										// complete!
										break;
									} else {
										// was something else apparently, but it is pasted, so keep it
										data ~= esc;
									}
								} else {
									data ~= '\033';
									data ~= cast(char) n;
								}
							} else {
								data ~= cast(char) n;
							}
						}
						return [InputEvent(PasteEvent(data))];
					case "\033[M":
						// mouse event
						auto buttonCode = _nextRaw() - 32;
						// nextChar is commented because i'm not using UTF-8 mouse mode
						// cuz i don't think it is as widely supported
						auto x = cast(int) (/*nextChar*/(_nextRaw())) - 33; /* they encode value + 32, but make upper left 1,1. I want it to be 0,0 */
						auto y = cast(int) (/*nextChar*/(_nextRaw())) - 33; /* ditto */
						
						
						bool isRelease = (buttonCode & 0b11) == 3;
						int buttonNumber;
						if(!isRelease) {
							buttonNumber = (buttonCode & 0b11);
							if(buttonCode & 64)
								buttonNumber += 3; // button 4 and 5 are sent as like button 1 and 2, but code | 64
							// so button 1 == button 4 here
							
							// note: buttonNumber == 0 means button 1 at this point
							buttonNumber++; // hence this
							
							
							// apparently this considers middle to be button 2. but i want middle to be button 3.
							if(buttonNumber == 2)
								buttonNumber = 3;
							else if(buttonNumber == 3)
								buttonNumber = 2;
						}
						
						auto modifiers = buttonCode & (0b0001_1100);
						// 4 == shift
						// 8 == meta
						// 16 == control
						
						MouseEvent m;
						
						if(buttonCode & 32)
							m.eventType = MouseEvent.Type.Moved;
						else
							m.eventType = isRelease ? MouseEvent.Type.Released : MouseEvent.Type.Pressed;
						
						// ugh, if no buttons are pressed, released and moved are indistinguishable...
						// so we'll count the buttons down, and if we get a release
						static int buttonsDown = 0;
						if(!isRelease && buttonNumber <= 3) // exclude wheel "presses"...
							buttonsDown++;
						
						if(isRelease && m.eventType != MouseEvent.Type.Moved) {
							if(buttonsDown)
								buttonsDown--;
							else // no buttons down, so this should be a motion instead..
								m.eventType = MouseEvent.Type.Moved;
						}
						
						
						if(buttonNumber == 0)
							m.buttons = 0; // we don't actually know :(
						else
							m.buttons = 1 << (buttonNumber - 1); // I prefer flags so that's how we do it
						m.position = ConsolePoint(cast(short)x, cast(short)y);
						m.modifierState = modifiers;
						
						return [InputEvent(m)];
					default:
						// look it up in the termcap key database
						//auto cap = terminal.findSequenceInTermcap(sequence);
						//if(cap !is null) {
						// return translateTermcapName(cap);
						/*} else*/ {
							if(_console.inFamily(ConsoleFamily.xterm)) {
								import std.conv, std.string;
								auto terminator = sequence[$ - 1];
								auto parts = sequence[2 .. $ - 1].split(";");
								// parts[0] and terminator tells us the key
								// parts[1] tells us the modifierState
								
								uint modifierState;
								
								int modGot;
								if(parts.length > 1)
									modGot = to!int(parts[1]);
							mod_switch: switch(modGot) {
									case 2: modifierState |= ModifierState.shift; break;
									case 3: modifierState |= ModifierState.alt; break;
									case 4: modifierState |= ModifierState.shift | ModifierState.alt; break;
									case 5: modifierState |= ModifierState.control; break;
									case 6: modifierState |= ModifierState.shift | ModifierState.control; break;
									case 7: modifierState |= ModifierState.alt | ModifierState.control; break;
									case 8: modifierState |= ModifierState.shift | ModifierState.alt | ModifierState.control; break;
									case 9:
										..
											case 16:
											modifierState |= ModifierState.meta;
										if(modGot != 9) {
											modGot -= 8;
											goto mod_switch;
										}
										break;
										
										// this is an extension in my own terminal emulator
									case 20:
										..
											case 36:
											modifierState |= ModifierState.windows;
										modGot -= 20;
										goto mod_switch;
									default:
								}
								
								switch(terminator) {
									case 'A': return keyPressAndRelease(SpecialKeyEvent.Key.UpArrow, modifierState);
									case 'B': return keyPressAndRelease(SpecialKeyEvent.Key.DownArrow, modifierState);
									case 'C': return keyPressAndRelease(SpecialKeyEvent.Key.RightArrow, modifierState);
									case 'D': return keyPressAndRelease(SpecialKeyEvent.Key.LeftArrow, modifierState);
										
									case 'H': return keyPressAndRelease(SpecialKeyEvent.Key.Home, modifierState);
									case 'F': return keyPressAndRelease(SpecialKeyEvent.Key.End, modifierState);
										
									case 'P': return keyPressAndRelease(SpecialKeyEvent.Key.F1, modifierState);
									case 'Q': return keyPressAndRelease(SpecialKeyEvent.Key.F2, modifierState);
									case 'R': return keyPressAndRelease(SpecialKeyEvent.Key.F3, modifierState);
									case 'S': return keyPressAndRelease(SpecialKeyEvent.Key.F4, modifierState);
										
									case '~': // others
										switch(parts[0]) {
											case "5": return keyPressAndRelease(SpecialKeyEvent.Key.PageUp, modifierState);
											case "6": return keyPressAndRelease(SpecialKeyEvent.Key.PageDown, modifierState);
											case "2": return keyPressAndRelease(SpecialKeyEvent.Key.Insert, modifierState);
											case "3": return keyPressAndRelease(SpecialKeyEvent.Key.Delete, modifierState);
												
											case "15": return keyPressAndRelease(SpecialKeyEvent.Key.F5, modifierState);
											case "17": return keyPressAndRelease(SpecialKeyEvent.Key.F6, modifierState);
											case "18": return keyPressAndRelease(SpecialKeyEvent.Key.F7, modifierState);
											case "19": return keyPressAndRelease(SpecialKeyEvent.Key.F8, modifierState);
											case "20": return keyPressAndRelease(SpecialKeyEvent.Key.F9, modifierState);
											case "21": return keyPressAndRelease(SpecialKeyEvent.Key.F10, modifierState);
											case "23": return keyPressAndRelease(SpecialKeyEvent.Key.F11, modifierState);
											case "24": return keyPressAndRelease(SpecialKeyEvent.Key.F12, modifierState);
											default:
										}
										break;
										
									default:
								}
							} else if(_console.inFamily(ConsoleFamily.rxvt)) {
								// FIXME: figure these out. rxvt seems to just change the terminator while keeping the rest the same
								// though it isn't consistent. ugh.
							} else {
								// maybe we could do more terminals, but linux doesn't even send it and screen just seems to pass through, so i don't think so; xterm prolly covers most them anyway
								// so this space is semi-intentionally left blank
							}
						}
				}
				
				return null;
			}
			
			auto c = _nextRaw(true);
			if(c == -1)
				return null; // interrupted; give back nothing so the other level can recheck signal flags
			if(c == 0)
				return [InputEvent(EndOfFileEvent())];
			if(c == '\033') {
				if(timedCheckForInput(50)) {
					// escape sequence
					c = _nextRaw();
					if(c == '[') { // CSI, ends on anything >= 'A'
						return doEscapeSequence(readEscapeSequence(sequenceBuffer));
					} else if(c == 'O') {
						// could be xterm function key
						auto n = _nextRaw();
						
						char[3] thing;
						thing[0] = '\033';
						thing[1] = 'O';
						thing[2] = cast(char) n;
						
						//auto cap = _console.findSequenceInTermcap(thing);
						//if(cap is null) {
							return charPressAndRelease('\033') ~
								charPressAndRelease('O') ~
									charPressAndRelease(thing[2]);
						//} else {
						//	return translateTermcapName(cap);
						//}
					} else {
						// I don't know, probably unsupported terminal or just quick user input or something
						return charPressAndRelease('\033') ~ charPressAndRelease(_nextUtf8Char(c));
					}
				} else {
					// user hit escape (or super slow escape sequence, but meh)
					return keyPressAndRelease(SpecialKeyEvent.Key.Escape);
				}
			} else {
					import std.stdio;
				writeln(c);
				// FIXME: what if it is neither? we should check the termcap
				auto next = _nextUtf8Char(c);
				if(next == 127) // some terminals send 127 on the backspace. Let's normalize that.
					next = '\b';
				return charPressAndRelease(next);
			}
		}
	}
}


alias Console = UnixConsole;
module console2.sys.windows;

version(Windows):
import core.sys.windows.windows;
import std.algorithm;
import std.conv;
import std.math;
import std.stdio;
import std.string;
import console2.common;



class WindowsConsole : AbstractConsole
{
    protected CONSOLE_SCREEN_BUFFER_INFO _info;
    package HANDLE _outputHandle = null;
	package HANDLE _inputHandle = null;
    protected ConsoleColor _defFg;
	protected ConsoleColor _defBg;
	package ConsolePoint _sizeCache;
    

    this(File outputStream = stdout)
    {
        super(outputStream);
		initialize();
		_cursor = new WindowsConsole.Cursor();
		_sizeCache = size;
    }    
    

	//-------------------------------------------------------------------------- GETTERS
	@property
	public override string name() const
	{
		return "windows";
	}


	@property
	public override ConsolePoint size()
	{
		GetConsoleScreenBufferInfo(_outputHandle, &_info);
		short cols, rows;
		
		cols = cast(short)(_info.srWindow.Right  - _info.srWindow.Left + 1);
		rows = cast(short)(_info.srWindow.Bottom - _info.srWindow.Top  + 1);

		_sizeCache = ConsolePoint(cols, rows);
		return _sizeCache;
	}


	//-------------------------------------------------------------------------- METHODS
	public override typeof(this) clear()
	{
		COORD start = {0,0};
		ConsolePoint size = this.size;
		FillConsoleOutputCharacterA( _outputHandle, ' ', size.w * size.h, start, null);
		this.cursor.position = ConsolePoint(0, 0);
		return this;
	}


	protected override void applyState(const ConsoleState state)
    {
		stdout.flush();
		SetConsoleTextAttribute(_outputHandle, buildColor(_state.foreground, _state.background));
    }
    
    
	private void initialize()
    {
		enum FG_MASK = 0x0f;
		enum BG_MASK = 0xf0;

        uint handle;
        
        if (_stream == stdout)
            handle = STD_OUTPUT_HANDLE;
        else if (_stream == stderr)
            handle = STD_ERROR_HANDLE;
        else
            assert(0, "Invalid console output stream specified (must be either stdout or stderr)");
                
        _outputHandle  = GetStdHandle(handle);
        _inputHandle   = GetStdHandle(STD_INPUT_HANDLE);
        
        // Get current colors
        GetConsoleScreenBufferInfo(_outputHandle, &_info);
        
        // Background are first 4 bits
		_defBg = toggleColorBits(cast(ConsoleColor)((_info.wAttributes & (BG_MASK)) >> 4));
                
        // Rest are foreground
		_defFg = toggleColorBits(cast(ConsoleColor)(_info.wAttributes & (FG_MASK)));
    }
    
    
    private ushort buildColor(ConsoleColor fg, ConsoleColor bg)
    {        
        if(fg == ConsoleColor.initial)
            fg = _defFg;
        
        if(bg == ConsoleColor.initial) 
            bg = _defBg;
            
		return cast(ushort)(toggleColorBits(fg) | toggleColorBits(bg) << 4);
    }
    
    private ConsoleColor toggleColorBits(ConsoleColor color)
    {
        // swap red and blue bits and apply bright (3rd bit on)
        return cast(ConsoleColor)(
			(color & ConsoleColor.green)
            | ((color & ConsoleColor.red)    ? color.blue : 0)
            | ((color & ConsoleColor.blue)   ? color.red : 0)
			| ((color & ConsoleColor.bright) ? 8 : 0)
			);
    }



	//-------------------------------------------------------------------------- CURSOR
	class Cursor : ConsoleCursor
	{
		protected CONSOLE_CURSOR_INFO _cursorInfo;

		this()
		{
			GetConsoleCursorInfo(_outputHandle, &_cursorInfo);
		}


		@property
		public override ConsolePoint position()
		{
			GetConsoleScreenBufferInfo(_outputHandle, &_info);
			return ConsolePoint(
				_info.dwCursorPosition.X, 
				//min(_info.dwCursorPosition.Y, height) // To keep same behaviour with posix
				_info.dwCursorPosition.Y, 
			);
		}


		@property
		public override bool visible()
		{
			GetConsoleCursorInfo(_outputHandle, &_cursorInfo);
			return cast(bool)_cursorInfo.bVisible;
		}


		@property
		public override ConsoleCursor visible(bool isVisible)
		{
			_cursorInfo.bVisible = isVisible;
			SetConsoleCursorInfo(_outputHandle, &_cursorInfo);

			return this;
		}

		@property 
		public override uint size()
		{
			GetConsoleCursorInfo(_outputHandle, &_cursorInfo);
			return cast(bool)_cursorInfo.dwSize;
		}

		@property 
		public override ConsoleCursor size(uint newSize)
		{
			_cursorInfo.dwSize = newSize;
			SetConsoleCursorInfo(_outputHandle, &_cursorInfo);

			return this;
		}
		
		public override Cursor moveTo(const ConsolePoint pos)
		{
			COORD coord = {
				cast(short)pos.x,
				cast(short)pos.y
			};
			
			SetConsoleCursorPosition(_outputHandle, coord);
			
			return this;
		}
		
		
		public override Cursor move(const ConsolePoint pos)
		{
			moveTo(position + pos);
			
			return this;
		}
	}


	static struct InputReader
	{
		protected ConsoleInputFlags _flags;
		protected Console _console;
		protected DWORD _oldInput;
		protected DWORD _oldOutput;
		protected InputEvent[] _inputQueue;


		this(Console console, ConsoleInputFlags flags)
		{
			_console = console;
			_flags = flags;

			GetConsoleMode(console._inputHandle, &_oldInput);
			GetConsoleMode(console._outputHandle, &_oldOutput);

			DWORD mode;

			mode |= ENABLE_PROCESSED_INPUT;
			if(flags & ConsoleInputFlags.size)
				mode |= ENABLE_WINDOW_INPUT;
			if(flags & ConsoleInputFlags.echo)
				mode |= ENABLE_ECHO_INPUT;
			if(flags & ConsoleInputFlags.mouse)
				mode |= ENABLE_MOUSE_INPUT;

			SetConsoleMode(console._inputHandle, mode);

			mode = 0;
			// we want this to match linux too
			mode |= ENABLE_PROCESSED_OUTPUT; /* 0x01 */
			mode |= ENABLE_WRAP_AT_EOL_OUTPUT; /* 0x02 */
			SetConsoleMode(console._outputHandle, mode);
		}


		void close()
		{
			SetConsoleMode(_console._inputHandle, _oldInput);
			SetConsoleMode(_console._outputHandle, _oldOutput);
		}


		InputEvent nextEvent() {
			_console.outputStream.flush();

			if(_inputQueue.length) {
				auto e = _inputQueue[0];
				_inputQueue = _inputQueue[1 .. $];
				return e;
			}
			
		wait_for_more:		
			auto more = readNextEvents();
			if(!more.length)
				goto wait_for_more; // i used to do a loop (readNextEvents can read something, but it might be discarded by the input filter) but now it goto's above because readNextEvents might be interrupted by a SIGWINCH aka size event so we want to check that at least
			
			assert(more.length);
			
			auto e = more[0];
			_inputQueue = more[1 .. $];
			return e;
		}


		InputEvent[] readNextEvents() {
			_console.outputStream.flush(); // make sure all output is sent out before waiting for anything
			
			INPUT_RECORD[32] buffer;
			DWORD actuallyRead;
			// FIXME: ReadConsoleInputW
			auto success = ReadConsoleInputA(_console._inputHandle, buffer.ptr, buffer.length, &actuallyRead);
			if(success == 0)
				throw new Exception("ReadConsoleInput");
			
			InputEvent[] newEvents;
		input_loop: foreach(record; buffer[0 .. actuallyRead]) {
				switch(record.EventType) {
					case KEY_EVENT:
						auto ev = record.KeyEvent;
						CharacterEvent e;
						SpecialKeyEvent ske;
						
						e.eventType = ev.bKeyDown ? CharacterEvent.Type.Pressed : CharacterEvent.Type.Released;
						ske.eventType = ev.bKeyDown ? SpecialKeyEvent.Type.Pressed : SpecialKeyEvent.Type.Released;
						
						// only send released events when specifically requested
						if(!(_flags & ConsoleInputFlags.winReleasedKeys) && !ev.bKeyDown)
							break;
						
						e.modifierState = ev.dwControlKeyState;
						ske.modifierState = ev.dwControlKeyState;
						
						if(ev.UnicodeChar) {
							e.character = cast(dchar) cast(wchar) ev.UnicodeChar;
							newEvents ~= InputEvent(e);
						} else {
							ske.key = cast(SpecialKeyEvent.Key) ev.wVirtualKeyCode;
							
							// FIXME: make this better. the goal is to make sure the key code is a valid enum member
							// Windows sends more keys than Unix and we're doing lowest common denominator here
							foreach(member; __traits(allMembers, SpecialKeyEvent.Key))
							{
								if(__traits(getMember, SpecialKeyEvent.Key, member) == ske.key) {
									newEvents ~= InputEvent(ske);
									break;
								}
							}
						}
						break;

					case MOUSE_EVENT:
						auto ev = record.MouseEvent;
						MouseEvent e;
						
						e.modifierState = ev.dwControlKeyState;
						e.position = ConsolePoint(ev.dwMousePosition.X, ev.dwMousePosition.Y);
						
						switch(ev.dwEventFlags) {
							case 0:
								//press or release
								e.eventType = MouseEvent.Type.Pressed;
								static DWORD lastButtonState;
								auto lastButtonState2 = lastButtonState;
								e.buttons = ev.dwButtonState;
								lastButtonState = e.buttons;
								
								// this is sent on state change. if fewer buttons are pressed, it must mean released
								if(cast(DWORD) e.buttons < lastButtonState2) {
									e.eventType = MouseEvent.Type.Released;
									// if last was 101 and now it is 100, then button far right was released
									// so we flip the bits, ~100 == 011, then and them: 101 & 011 == 001, the
									// button that was released
									e.buttons = lastButtonState2 & ~e.buttons;
								}
								break;
							case MOUSE_MOVED:
								e.eventType = MouseEvent.Type.Moved;
								e.buttons = ev.dwButtonState;
								break;
							case 0x0004/*MOUSE_WHEELED*/:
								e.eventType = MouseEvent.Type.Pressed;
								if(ev.dwButtonState > 0)
									e.buttons = MouseEvent.Button.ScrollDown;
								else
									e.buttons = MouseEvent.Button.ScrollUp;
								break;
							default:
								continue input_loop;
						}
						
						newEvents ~= InputEvent(e);
						break;

					case WINDOW_BUFFER_SIZE_EVENT:
						auto ev = record.WindowBufferSizeEvent;
						auto oldSize = _console._sizeCache;
						auto newSize = ConsolePoint(ev.dwSize.X, ev.dwSize.Y);
						newEvents ~= InputEvent(SizeChangedEvent(oldSize, newSize));
						break;
						// FIXME: can we catch ctrl+c here too?
					default:
						// ignore
				}
			}
			
			return newEvents;
		}
	}
}

alias Console = WindowsConsole;
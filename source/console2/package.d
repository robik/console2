module console2;

import std.stdio;
public import console2.common;


version(Windows) {
    public import console2.sys.windows;
}
else version(linux) {
    public import console2.sys.unix;
}
else {
    static assert(false, "Unsupported platform");
}


public __gshared Console console;


shared static this()
{
    console = new Console(stdout);
}

shared static ~this()
{
	if (console.restoreAtExit)
		console.reset();
}
